PatternMaker {

	*new { arg inst, gap = Pseq([1/4, 1/4, 1/4, 1/4], inf), stretch = 1, buffers = [0, 1], rate = Pwhite(0.7, 1.2, inf), pan = Prand([-1, 0, 1], inf), startpos = Pwhite(0.0, 0.7, inf), duration = Pwhite(1/4, 1.6, inf), amp = 1;
		var pat;

		pat = Pbind(
		'instrument', inst,
		'dur', gap,
		'stretch', stretch,
		'buffer', Prand(buffers, inf),
		'rate', rate,
		'pan', pan,
		'startPos', startpos,
		'duration', duration,
		'amp', amp
	).asStream;

		^pat;
}

	*newRandWhite { arg inst, buffers = [0, 1], amp = 1;
		var pat;

		pat = Pbind(
		'instrument', inst,
		'dur', Pwhite(0.01, rrand(0.2, 2), inf),
		'stretch', 1,
		'buffer', Prand(buffers, inf),
		'rate', Pwhite(0.01, rrand(0.2, rrand(1, 5)), inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pwhite(0.0, rrand(0.3, 0.9), inf),
		'duration', Pwhite(0.01, rrand(0.2, 5), inf),
		'amp', amp
	).asStream;

		^pat;
	}

	*newRandBrown { arg inst, buffers = [0, 1], amp = 1, step = 0.125;
		var pat, gapmax;

		gapmax = rrand(0.2, 2);
		"Max Gap:".postln;
		gapmax.postln;
		pat = Pbind(
		'instrument', inst,
		'dur', Pbrown(0.01, rrand(0.2, 2), step, inf),
		'stretch', 1,
		'buffer', Prand(buffers, inf),
		'rate', Pbrown(0.01, rrand(0.5, rrand(1, 5)), step, inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pbrown(0.0, rrand(0.3, 0.9), step, inf),
		'duration', Pbrown(0.01, rrand(0.2, 5), step, inf),
		'amp', amp
	).asStream;

		^pat;
	}

	*newRandExp { arg inst, buffers = [0, 1], amp = 1;
		var pat, gapmax;

		gapmax = rrand(0.2, 2);
		"Max Gap:".postln;
		gapmax.postln;
		pat = Pbind(
		'instrument', inst,
		'dur', Pexprand(rrand(0.001, 0.19), rrand(0.2, 2), inf),
		'stretch', 1,
		'buffer', Prand(buffers, inf),
		'rate', Pexprand(0.01, rrand(0.5, rrand(1, 5)), inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pexprand(rrand(0.0001, 0.25), rrand(0.3, 0.9), inf),
		'duration', Pexprand(rrand(0.001, 0.19), rrand(0.2, 6), inf),
		'amp', amp
	).asStream;

		^pat;
	}

}

// Great job! 10/10