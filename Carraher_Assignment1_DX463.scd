//---------------------------------------------------------------
//BUFFERS
//---------------------------------------------------------------

(
s.waitForBoot({
	var buffers1, buffers2;
	var pattern1, pattern2, pattern3;
	var synth, verbbus, reverb, verbnote, verbmix = 0.33, verbroom = 0.33;
	var func, func2, func3, proc1, proc2, proc3, proc4;
	var playEvent1, stopEvent1, playEvent2, stopEvent2, playEvent3, stopEvent3;
	var slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8;

	buffers1 = [];
	PathName("/Users/ryancarraher/Desktop/UW/Classes/Autumn 2020/DX463/Assignment1/Bassoon/").filesDo{arg pathname;
		buffers1 = buffers1.add(CtkBuffer(pathname.fullPath).load)
	};


	buffers2 = [];
	PathName("/Users/ryancarraher/Desktop/UW/Classes/Autumn 2020/DX463/Assignment1/springdrum/").filesDo{arg pathname;
		buffers2 = buffers2.add(CtkBuffer(pathname.fullPath).load)
	};

	verbbus = CtkAudio.new(2);



	//---------------------------------------------------------------
	//SYNTH DEF AND PATTERNS
	//---------------------------------------------------------------

	synth = CtkProtoNotes(
		SynthDef('plbuf', {
			arg buffer = 440, amp = 0.75, rate = 1.0, startPos = 0, duration, pan = 0, out = 0;
			var sig, env;
			env = EnvGen.kr(Env.perc(0.2, duration - 0.2, amp));
			sig = PlayBuf.ar(2, buffer, rate * BufRateScale.kr(buffer), 0, startPos * BufSampleRate.kr(buffer));
			sig = Pan2.ar(sig, pan);
			Out.ar(verbbus, sig * env)
		}),
	);

	reverb = CtkSynthDef('reverb', {arg in = 0, out = 0, mix = 0.33, room = 0.33;
    var snd, env;
    snd = In.ar(verbbus, 2);
    Out.ar(out, FreeVerb.ar(snd, mix, room));
});

	verbnote = reverb.note(addAction: 'tail', target: 0).play;

	pattern1 = Pbind(
		'instrument', 'plbuf',
		'dur', Pseq([1/4, 1/4, 1/4, 1/4], inf),
		'stretch', 1.0,
		'buffer', Prand(buffers1, inf),
		'rate', Pwhite(0.7, 1.2, inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pwhite(0.0, 0.7, inf),
		'duration', Pwhite(1/4, 1.6, inf),
		'amp', 1.0
	).asStream;


	pattern2 = Pbind(
		'instrument', 'plbuf',
		'dur', Pwhite(0.2, 1, inf),
		'stretch', 1.0,
		'buffer', Prand(buffers2, inf),
		'rate', Pwhite(0.7, 1.2, inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pwhite(0.0, 0.7, inf),
		'duration', Pwhite(0.3, 4, inf),
		'amp', 1.0
	).asStream;

	pattern3 = Pbind(
		'instrument', 'plbuf',
		'dur', Pseq([1/4, 1/4, 1/4, 1/4], inf),
		'stretch', 1.0,
		'buffer', Prand(buffers2, inf),
		'rate', Pwhite(0.7, 1.2, inf),
		'pan', Prand([-1, 0, 1], inf),
		'startPos', Pwhite(0.0, 0.7, inf),
		'duration', Pwhite(1/4, 1.6, inf),
		'amp', 1.0
	).asStream;


	//---------------------------------------------------------------
	//SLIDERS and GLOBAL
	//---------------------------------------------------------------
	~durfreq; //Max duration value for Pwhite in func

	slider1 = CCResponder({ |src,chan,num,val|
		~durfreq = (val.linlin(0, 127, 0.2, 5));
		"Max Duration [slider 1] is: ".postln;
		~durfreq.postln;
	},nil,nil,74,nil);


	~ratemax; //Max rate value for func playback

	slider2 = CCResponder({ |src,chan,num,val|
		~ratemax = (val.linlin(0, 127, 0.8, 3));
		"Max Rate [slider 2] is: ".postln;
		~ratemax.postln;
	},nil,nil,71,nil);

	~amp1; //volume slider for func

	slider3 = CCResponder({ |src,chan,num,val|
		~amp1 = (val.linlin(0, 127, 0, 1));
		"This Amp [slider 3] is: ".postln;
		~amp1.postln;
	},nil,nil,91,nil);

	~durfreq2; //Max duration value for Pwhite in func3

	slider4 = CCResponder({ |src,chan,num,val|
		~durfreq2 = (val.linlin(0, 127, 0.2, 5));
		"Max Duration [slider 4] is: ".postln;
		~durfreq2.postln;
	},nil,nil,93,nil);


	~ratemax2; //Max rate value for func3 playback

	slider5 = CCResponder({ |src,chan,num,val|
		~ratemax2 = (val.linlin(0, 127, 0.8, 3));
		"Max Rate [slider 5] is: ".postln;
		~ratemax2.postln;
	},nil,nil,73,nil);

	~amp2; //volume slider for func3

	slider6 = CCResponder({ |src,chan,num,val|
		~amp2 = (val.linlin(0, 127, 0, 1));
		"This Amp [slider 6] is: ".postln;
		~amp2.postln;
	},nil,nil,72,nil);

	//controls reverb mix
	slider7 = CCResponder({ |src,chan,num,val|
		verbmix = (val.linlin(0, 127, 0, 1));
		"Reverb Mix [slider 7] is: ".postln;
		verbmix.postln;
		verbnote.mix_(verbmix)
	},nil,nil,5,nil);

	//controls reverb room size
	slider8 = CCResponder({ |src,chan,num,val|
		verbroom = (val.linlin(0, 127, 0, 1));
		"Reverb Room [slider 8] is: ".postln;
		verbroom.postln;
		verbnote.room_(verbroom)
	},nil,nil,84,nil);



	//---------------------------------------------------------------
	//FUNCTIONS
	//---------------------------------------------------------------

	//Sliders 1, 2, 3 apply to func
	func = {arg pattern, stretchControl, out, group;
		var dur, event, next, currentStretch;
		Task({
			loop({
				var oscFunc, streamIn, streamOut, stream, rateStreamIn, rateStreamOut, rateStream;
					streamIn = Pwhite(0.1, ~durfreq, inf);
	            streamOut = streamIn.asStream;
				stream = streamOut.value;
				rateStreamIn = Pwhite(0.7, ~ratemax, inf);
				rateStreamOut = rateStreamIn.asStream;
				rateStream = rateStreamOut.value;
				oscFunc = OSCFunc({arg msg, time;
					currentStretch = msg[2];
					oscFunc.free;
				}, '/c_set');
				s.sendMsg('/c_get', stretchControl.bus);
				s.sync;

				event = pattern.next(Event.new);
				dur = event['duration'];
				next = stream * event['stretch'] * currentStretch;
				synth[event['instrument']].note(duration: dur, target: group)
				.buffer_(event['buffer'])
				.amp_(~amp1)
				.pan_(event['pan'])
				.out_(out)
				.rate_(rateStream)
				.startPos_(event['startPos'] * event['buffer'].duration)
				.duration_(dur).play;
				next.wait;
			})
		});
	};

    //just takes from the pattern...only reverb sliders apply
	func2 = {arg pattern, stretchControl, out, group;
		var dur, event, next, currentStretch;
		Task({
			loop({
				var oscFunc;
				oscFunc = OSCFunc({arg msg, time;
					currentStretch = msg[2];
					oscFunc.free;
				}, '/c_set');
				s.sendMsg('/c_get', stretchControl.bus);
				s.sync;

				event = pattern.next(Event.new);
				dur = event['duration'];
				next = event['dur'] * event['stretch'] * currentStretch;
				synth[event['instrument']].note(duration: dur, target: group)
				.buffer_(event['buffer'])
				.amp_(event['amp'])
				.pan_(event['pan'])
				.out_(out)
				.rate_(event['rate'])
				.startPos_(event['startPos'] * event['buffer'].duration)
				.duration_(dur).play;
				next.wait;
			})
		});
	};

	//Sliders 4,5,6 apply to func3
	func3 = {arg pattern, stretchControl, out, group;
		var dur, event, next, currentStretch;
		Task({
			loop({
				var oscFunc, streamIn, streamOut, stream, rateStreamIn, rateStreamOut, rateStream;
					streamIn = Pwhite(0.1, ~durfreq2, inf);
	            streamOut = streamIn.asStream;
				stream = streamOut.value;
				rateStreamIn = Pwhite(0.7, ~ratemax2, inf);
				rateStreamOut = rateStreamIn.asStream;
				rateStream = rateStreamOut.value;
				oscFunc = OSCFunc({arg msg, time;
					currentStretch = msg[2];
					oscFunc.free;
				}, '/c_set');
				s.sendMsg('/c_get', stretchControl.bus);
				s.sync;

				event = pattern.next(Event.new);
				dur = event['duration'];
				next = stream * event['stretch'] * currentStretch;
				synth[event['instrument']].note(duration: dur, target: group)
				.buffer_(event['buffer'])
				.amp_(~amp2)
				.pan_(event['pan'])
				.out_(out)
				.rate_(rateStream)
				.startPos_(event['startPos'] * event['buffer'].duration)
				.duration_(dur).play;
				next.wait;
			})
		});
	};

	//---------------------------------------------------------------
	//EVENTS
	//---------------------------------------------------------------


	proc1 = ProcModR.new(Env([0, 1, 0], [1, 5], 'sin', 1), 0.5, 2, 0, 'event1', server: s).function_({arg group, routebus, server, pmod;
		func.(pattern2, CtkControl.lfo(SinOsc, 0.15, 0.5, 2.0).play, out: routebus, group: group).play;
	});

	proc2 = ProcModR.new(Env([0, 1, 0], [1, 5], 'sin', 1), 0.5, 2, 0, 'event2', server: s).function_({arg group, routebus, server, pmod;
		func3.(pattern1, CtkControl.lfo(SinOsc, 0.50, 0.5, 3.0).play, out: routebus, group: group).play;
	});

	proc3 = ProcModR.new(Env([0, 1, 0], [5, 10], 'sin', 1), 0.5, 2, 0, 'event3', server: s).function_({arg group, routebus, server, pmod;
		func2.(pattern3, CtkControl.lfo(SinOsc, 0.2, 0.5, 5.0).play, out: routebus, group: group).play;
	});

	proc4 = ProcModR.new(Env([0, 1, 0], [3, 10], 'sin', 1), 0.5, 2, 0, 'event4', server: s).function_({arg group, routebus, server, pmod;
		func2.(pattern1, CtkControl.lfo(SinOsc, 0.7, 0.5, 2.0).play, out: routebus, group: group).play;
	});


	//---------------------------------------------------------------
	//MIDI
	//---------------------------------------------------------------

	MIDIIn.connectAll;
/*	MIDIFunc.trace;
	MIDIFunc.trace(false);*/

	//Button F10
	playEvent1 = ProgramChangeResponder({ |src,chan,val|
		"Event 1 has started".postln;
       proc1.play
    },nil,nil,0);

	//Button F11
	stopEvent1 = ProgramChangeResponder({ |src,chan,val|
		"Stopping event 1...".postln;
       proc1.release
    },nil,nil,1);

	//Button F12
	playEvent2 = ProgramChangeResponder({ |src,chan,val|
	"Event 2 has started".postln;
    proc2.play
    },nil,nil,2);

	//Button F13
	stopEvent2 = ProgramChangeResponder({ |src,chan,val|
	"Stopping Event 2...".postln;
    proc2.release
    },nil,nil,3);

	//Button F14
	playEvent3 = ProgramChangeResponder({ |src,chan,val|
	"Event 3 has started".postln;
    proc3.play
    },nil,nil,4);

	//Button F15
	stopEvent3 = ProgramChangeResponder({ |src,chan,val|
	"Stopping Event 3...".postln;
    proc3.release
    },nil,nil,5);
})
)

// GRADE
// 10
